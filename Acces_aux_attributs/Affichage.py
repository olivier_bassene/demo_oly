class Personne:

    def __init__(self,name, age):
        self.name = name
        self.age = age
        self.adresse = "paris"

    @property

    def age(self):
        return self._age

    @age.setter
    def age(self, value):
        if value >= 0:
            self._age = value
        else:
            self._age = 0

    def __str__(self):

        return "{} a {} ans et habite à {}".format(self.name,self.age,self.adresse)


class Protege:


    def __init__(self):
        self.a = 1
        self.b = 2
        self.c = 4

    def __getattr__(self, item):
        """Si Python ne trouve pas l'attribut nommé nom, il appelle
        cette méthode. On affiche une alerte"""

        print("Alerte ! Il n'y a pas d'attribut {} ici !".format(item))

if __name__ =="__main__":

    per = Personne("olivier", 22)
    #print(per)
    pro = Protege()
    print(pro.e)