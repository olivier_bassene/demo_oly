class Animal:
    n_animals = 0
    class_name = "Animal"

    def __init__(self, name, age):
        self.name = name
        self.age = age
        Animal.n_animals += 1

    def happy_birthday(self):
        self.age += 1

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, value):
        if value >= 0:
            self._age = value
        else:
            self._age = 0

    @staticmethod
    def get_n_animals():
        return Animal.n_animals

    def presentation(self):
        print(f"Hi, I am {self.name}!")

    def __repr__(self):
        return "l'animal {} a {} ans".format(self.name, self._age)
    
if __name__ == "__main__":
    bob = Animal("bob", -1)
    print(bob.n_animals)
    bob.presentation()
    # jeb = Animal("Jeb", 3)
    # print(Animal.n_animals)
    # print(Animal.get_n_animals())