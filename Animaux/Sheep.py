from demo_oly.Animaux.Animal import Animal
import  json

class Sheep(Animal):
    sheep_content = ["head", "body", "legs"]


    def __init__(self,name,age,has_wool):
        super().__init__(name,age)
        self.has_wool = True
        pass
    def __repr__(self):
        return "l'animal {} a {} ans et a des poiles({})".format(self.name, self._age, self.has_wool)

    def presentation(self):
        super(Sheep, self).presentation()
        print(f"I am a {self.age} years old Sheep with {'wool' if self.has_wool else 'no wool'}!")

    def __add__(self, other):
        return Sheep(self.name + other.name, self.age + other.age, self.has_wool + other.has_wool )

    def __next__(self):
        for i in Sheep.sheep_content:
            yield i

    def __iter__(self):
        return  self.__next__()

    def as_dic(self):
        return {"name": self.name,"age":self.age,"has_wood":self.has_wool}

    def __str__(self):
        return  json.dumps(self.as_dic())


if __name__ == "__main__":

    mouton = Sheep("mouton", 2,True)
    mouton2 = Sheep("mouton_mouton", 3, False)
    print(mouton)
    print(mouton.presentation())
    print(mouton2.presentation())
    Mouton = mouton + mouton2
    print(Mouton.presentation())
    for i in mouton:
        print(i)
    print(mouton.as_dic())
    shepp_j =str(mouton)
    dict_step = json.loads(shepp_j)

    re = Sheep(**(dict_step))
    print(re.presentation())


