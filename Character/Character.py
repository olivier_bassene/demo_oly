class Character:
    n_character = 0
    def __init__(self, name,hp,armor):
        self.name = name
        self.hp = hp
        self.armor = armor
        Character.n_character += 1

    @property
    def hp(self):
        return self._hp

    @hp.setter
    def hp(self, value):
        if value < 0:
            self._hp = 0
        else:
            self._hp = value

if __name__ == "__main__":
    print(Character.n_character)
    joueur = Character("sudoku",-20,"arme")
    print(Character.n_character)
    joueur2 = Character("sudoku1", 2, "arme1")
    print(joueur.hp)
    print(Character.n_character)