class Duree:


    def __init__(self,h = 0,min =0, sec = 0):

        self.min = min
        self.sec = sec
        self.h = h

    def __str__(self):

        return "{} heures: {} minutes: {} secondes".format(self.h,self.min, self.sec)

    def __getattr__(self, item):

        print("cet attribut n'existe pas".format(item))

    @property
    def min(self):
        return self._min
    @property
    def sec(self):
        return self._sec
    @min.setter
    def min(self, value):
        if value >=0:
            self._min = value
        else:
            self._min = 0

    @sec.setter
    def sec(self, value):
        if value >=0:
            self._sec= value
        else:
            self._sec = 0

    def __radd__(self, other):
        Nouvelle_Duree = Duree()
        Nouvelle_Duree.min =  self.min
        Nouvelle_Duree.sec = self.sec
        Nouvelle_Duree.h = self.h

        Nouvelle_Duree.sec += other

        if Nouvelle_Duree.sec >= 60:
            Nouvelle_Duree.min += (Nouvelle_Duree.sec // 60)
            Nouvelle_Duree.sec = (Nouvelle_Duree.sec%60)
        if Nouvelle_Duree.min >=60:
            Nouvelle_Duree.h += Nouvelle_Duree.min // 60
            Nouvelle_Duree.min = Nouvelle_Duree.min  % 60
        return  Nouvelle_Duree


    def __eq__(self, other):

            print("A __eq__ called")
            return self.min == other.min and self.sec == other.sec
    def __gt__(self, other):
        return  self.min > other.min and self.sec > other.sec


if __name__ == "__main__":
    d = Duree(2,57,59)
    print(d)
    d2 = 235 + d
    print(d2)
    d3 = d2.__radd__(9825)

    print(d3)
    print(d2.__eq__(d3))
    print(d2.__gt__(d3))
