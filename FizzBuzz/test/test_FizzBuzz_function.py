from FizzBuzz.FizzBuzz_function import *
import pytest

def test_divisible_par_trois():
    """
    cette fonction permet de tester si un nombre est
    divisible par 3
    :return: le resultat du test
    """

    assert fizzbuzz(6) == "Fizz !"

def test_divisible_par_cinq():
    """
   cette fonction permet de tester si un nombre est
   divisible par 5
   :return: le resultat du test
   """

    assert fizzbuzz(10) == "Buzz !"

def test_divisible_par_cinq_et_trois():

    assert fizzbuzz(15) ==  "FizzBuzz"



"""
def test_imprime_chiffre():
 

    # Given
    expected = [1,2,"Fizz !",4,"Buzz !"]
    # Then
    actual = Imprimer_chiffre(5)
    # Then
    assert actual == expected

"""
print(Imprimer_chiffre(30))