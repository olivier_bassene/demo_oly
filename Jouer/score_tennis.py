class Score:

    def __init__(self,score_j1 =0, score_j2= 0, avantage_j1 =False, avantage_j2 =False, winner =""):

        self.score_j1 = score_j1
        self.score_j2 = score_j2
        self.avantage_j1 = avantage_j1
        self.avantage_j2 = avantage_j2
        self.winner = winner


    def ajouter_point_jouer_1(self):
        if self.score_j1 == 0:
            self.score_j1 = 15
        elif self.score_j1 == 15:
            self.score_j1 = 30
        elif self.score_j1 == 30:
            self.score_j1 = 40
        elif self.avantage_j1 is True:
            self.winner = "player 1"
        elif self.score_j2 == 40:
            self.winner = "egalité"
            if self.avantage_j1 == True:
                self.winner = "player 1"
            else:
                self.winner = "egalité"


    def ajouter_point_jouer_2(self):
        if self.score_j2 == 0:
            self.score_j2 = 15
        elif self.score_j2 == 15:
            self.score_j2 = 30
        elif self.score_j2 == 30:
            self.score_j2 = 40
        elif self.avantage_j2 is True:
            self.winner = "player 2"
        elif self.score_j1 == 40:
            self.winner = "egalité"
            if self.avantage_j2 == True:
                self.winner = "player 2"
            else:
                self.winner = "egalité"


if __name__ =="__main__":

    joeur = Score(avantage_j2=True)

    for i in range(2):
        joeur.ajouter_point_jouer_1()
        joeur.ajouter_point_jouer_2()
    print(joeur.score_j2)
    print(joeur.score_j1)
    print(joeur.avantage_j1)
    print(joeur.avantage_j2)
    print(joeur.winner)
