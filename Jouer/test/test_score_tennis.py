import pytest
from Jouer.score_tennis import Score
import numpy

def test_score_joueur_bien_renseinger():

    joueur = Score()

    assert joueur.score_j1  == 0
    assert joueur.score_j2 ==0

def test_joueur_1_marque_quinze_point():
    joueur =Score(score_j1 = 0, score_j2 = 0, avantage_j1 = False, avantage_j2 = False, winner = "")
    joueur.ajouter_point_jouer_1()
    assert joueur.score_j1 == 15



def test_joueur_1_marque_trente_point():
    joueur =Score(score_j1 = 15, score_j2 = 0, avantage_j1 = False, avantage_j2 = False, winner = "")
    joueur.ajouter_point_jouer_1()
    assert joueur.score_j1 == 30

def test_joueur_1_marque_quarante_point():
    joueur =Score(score_j1 = 30, score_j2 = 0, avantage_j1 = False, avantage_j2 = False, winner = "")
    joueur.ajouter_point_jouer_1()
    assert joueur.score_j1 == 40


def test_joueur_2_marque_quarante_point():
    joueur =Score(score_j1 = 0, score_j2 = 30, avantage_j1 = False, avantage_j2 = False, winner = "")
    joueur.ajouter_point_jouer_2()
    assert joueur.score_j2 == 40

def test_joueur_1_gagne_la_partie():
    joueur = Score(score_j1=40, score_j2=30, avantage_j1=True, avantage_j2=False, winner="")
    joueur.ajouter_point_jouer_1()
    assert joueur.winner ==  "player 1"
def test_egaite_joeur1_et_joueur2():
    joueur = Score(score_j1=40, score_j2=40, avantage_j1=False, avantage_j2=False, winner="")
    joueur.ajouter_point_jouer_1()
    assert joueur.winner ==  "egalité"


def test_egalite_et_avantage_jouer_1():
    joueur = Score(score_j1=40, score_j2=40, avantage_j1=True, avantage_j2=False, winner="")
    joueur.ajouter_point_jouer_1()
    assert joueur.winner ==  "player 1"

