class  LigneInventaire:

    """
   Classe représentant une ligne d'un inventaire de vente.

    Attributs attendus par le constructeur :
        produit -- le nom du produit
        prix -- le prix unitaire du produit
        quantite -- la quantité vendue du produit.
    """

    def __init__(self, produit, prix, quantite):
        self.produit = produit
        self.prix = prix
        self.quantite = quantite

    def __repr__(self):
        return  "<Ligne d'inventaire {} ({}X{})>".format(
                self.produit, self.prix, self.quantite)



if __name__ == "__main__":
    # Création de l'inventaire
    from operator import attrgetter,itemgetter
    inventaire = [
        LigneInventaire("pomme rouge", 1.2, 19),
        LigneInventaire("orange", 1.4, 24),
        LigneInventaire("banane", 0.9, 21),
        LigneInventaire("poire", 1.2, 24),
    ]
    print(sorted(inventaire, key = attrgetter("prix", "quantite")))

    print(inventaire.sort(key = attrgetter("quantite"), reverse=True))
    print(sorted(inventaire, key = attrgetter("prix")))