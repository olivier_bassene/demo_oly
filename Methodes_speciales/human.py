class Human:
    """
    controler la representation
    """
    def __init__(self,prenom, nom):

        self.prenom =prenom
        self.nom = nom
        self.age = 10
        self.adresse = "paris"

    def __repr__(self):
        return "Personne:  nom ({}), prenom ({}), age ({}), adresse ({})".format(self.nom, self.prenom, \
                                                                                 self.age, self.adresse)

    def __getattr__(self, item):
        """
        Si Python ne trouve pas l'attribut nommé nom, il appelle
        cette méthode. On affiche une alerte


        """
        print(" Attention l'attribut {} n'existe pas".format(item))

class Duree:

    def __init__(self, heure = 0, min = 0, sec= 0):
        self.heure = heure
        self.minute = min
        self.seconde =  sec

    def __str__(self):
             """Affichage un peu plus joli de nos objets"""

             return " On a : {} heure :{} minutes :{} secondes".format(self.heure, self.minute, self.seconde)

    def __radd__(self, other):
        nouvelle_duree = Duree()
        # On va copier self dans l'objet créé pour avoir la même durée
        nouvelle_duree.minute = self.minute
        nouvelle_duree.seconde = self.seconde
        nouvelle_duree.heure = self.heure

        # on ajouter des secondes a notre objet creer
        nouvelle_duree.seconde  += other
        if nouvelle_duree.seconde >= 60:
            nouvelle_duree.minute += int(nouvelle_duree.seconde /60)
            nouvelle_duree.seconde = nouvelle_duree.seconde % 60
        if nouvelle_duree.minute >= 60:
            nouvelle_duree.heure +=  int(nouvelle_duree.minute/ 60)
            nouvelle_duree.minute = int(nouvelle_duree.minute % 60)
        return  nouvelle_duree

    def __eq__(self, other):
        return self.seconde == other.seconde  and self.minute == other.minute

    def __gt__(self, autre_duree):
        """Test si self > autre_duree"""
        # On calcule le nombre de secondes de self et autre_duree
        nb_sec1 = self.seconde + self.minute * 60
        nb_sec2 = autre_duree.seconde + autre_duree.minute * 60
        return nb_sec1 > nb_sec2


if __name__== "__main__":
    Dur = Duree(4,59,59)
    print(Dur)
    print(Dur == Duree(7,59,59))
    print(Dur > Duree(7,59,59))
    print(123 + Dur )


    Personne1 = Human("olivier", "celestin")
    print(Personne1)
    print(Personne1.Village)