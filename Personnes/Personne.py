class Personne:
    nb_personne = 0

    def __init__(self, nom, prenom, adresse,age):
        self.nom =nom
        self.prenom = prenom
        self.adresse =adresse
        self.age = age
        Personne.nb_personne += 1

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, value):
        if value < 0:
            self._age =0
        else:
            self._age = value

    def bouger(self,adresse):
        self.adresse = adresse
        print("demenagement à {}".format(self.adresse))



if __name__ == "__main__":
    humain = Personne("bassene", "olivier","deuil la barre",-2)
    print(humain.nom)
    print(Personne.nb_personne)
    print(humain.prenom)
    print(humain.adresse)
    print(humain.age)
    humain.bouger("strasbourg")
    print("**"*6)
    humain1 = Personne("Diatta", "Lena", "cergy", 22)
    print(humain1.nom)
    print(Personne.nb_personne)
    print(humain1.prenom)
    print(humain1.adresse)
    print(humain1.age)