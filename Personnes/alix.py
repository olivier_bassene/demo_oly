def trap(height):
    i = 0
    l = len(height) # longueur de la liste
    save = 0 # resultat cherché
    while i < l - 1:
        h_max = max(height[i + 1:])
        h = height[i]
        for j, n in list(enumerate(height))[i + 1:]:
            if n > h or n == h_max:
                save += min([h, n]) * (j - i - 1) - sum(height[i + 1:j])
                i = j - 1
                break
        i += 1
    return save

print(trap([0,1,0]))

print(trap([0,1,0,2,1,0,1,3,2,1,2,1]))