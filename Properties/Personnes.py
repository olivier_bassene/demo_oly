class Personnes:
    """
    cette classe permet de définir une personne et ses caracteristiques
    """


    def __init__(self, nom, prenom):
        """
        constructeur de la classe
        :param nom:  le nom de la personne
        :param prenom:  le prenom de la personne
        """
        self.nom = nom
        self.prenom = prenom
        self.age = 33
        self._lieu_de_residence = "Paris"

    def _get_lieu_de_residence(self):
        """
        cette methode donne accès à l'attribut lieu_de_residence
        :return:  le lieu_de_residence
        """
        print("on accede  à  l'attribut lieu_de_residence")
        return self._lieu_de_residence

    def _set_lieu_de_residence(self, nouveau_lieu_de_residence):
        """
        cette méthode permet de modifier
        :return: la nouvelle adresse
        """
        print("attention, il semble {} a déménagé à {}".format(self.prenom, nouveau_lieu_de_residence))
        self._lieu_de_residence = nouveau_lieu_de_residence

    lieu_de_residence = property(_get_lieu_de_residence,_set_lieu_de_residence)

if __name__ == "__main__":
    personne1 = Personnes("olivier", "bassene")
    print(personne1.prenom)
    print(personne1.nom)
    print(personne1.age)
    print(personne1.lieu_de_residence)
    personne1.lieu_de_residence = "Berlin"
    print(personne1.lieu_de_residence)