# Demo_Oly

## Installation

Keep calm and clone the project.

## Usage

Read the doc!

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://gitlab.com/olivier_bassene/demo_oly/-/blob/master/LICENCE)
## Authors

* **BASSENE OLIVIER ** - *Initial work* -  [olivier_bassene](https://gitlab.com/olivier_bassene)