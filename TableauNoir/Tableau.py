class Tableau:
    """
    Classe qui modélise un tableau.
    """
    nb_tableau = 0
    def __init__(self,largeur):
        """

        :param largeur: la lageur du tableau

        :param surface : partie sur laquelle on doit ecrire
        """
        self.surface = ""
        self.largeur =largeur
        Tableau.nb_tableau += 1

    @property
    def largeur(self):
        return self._largeur
    @largeur.setter
    def largeur(self, value):
        if value< 0:
            self._largeur = 0
        else:
            self._largeur = value


    def ecrire(self, message_a_ecrire):
        """Méthode permettant d'écrire sur la surface du tableau.
        Si la surface n'est pas vide, on saute une ligne avant de rajouter
        le message à écrire"""

        if self.surface != "":
            self.surface += "\n"
        self.surface += message_a_ecrire

    def lire(self):
        print(self.surface)
    def effacer(self):
        self.surface = ""
    def combien(cls):
        """
        methode de classe qui permet de compter combien de tableau on a créer
        :return: nombre de tableau créer
        """
        print("le nombre de tableau crees est {}".format(cls.nb_tableau))
    combien = classmethod(combien)

    def afficher():
        """
        methode statique
        :return: permet juste d'afficher un resultats
        """
        print("afficher la même chose ")
        print("peu importe les données de l'objet ou de la classe.")
        afficher = staticmethod(afficher)
if __name__ == "__main__":
    Tableau.combien()
    premier_tab = Tableau(-2)
    premier_tab.lire()
    print(premier_tab._largeur)
    premier_tab.ecrire("je suis a strasbourg")
    premier_tab.lire()
    premier_tab.ecrire("avec mon frere")
    premier_tab.lire()
    premier_tab.effacer()
    premier_tab.lire()

    premier_tab1 = Tableau(-2)
    premier_tab1.lire()
    print(premier_tab1._largeur)
    premier_tab1.ecrire("je suis a strasbourg")
    premier_tab1.lire()
    premier_tab1.ecrire("avec mon frere")
    premier_tab1.lire()
    premier_tab1.effacer()
    premier_tab1.lire()
    Tableau.combien()
    print(dir(premier_tab))
    print(premier_tab.__dict__)
