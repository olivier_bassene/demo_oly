import pytest
import numpy
from Tennis_Function import TableauDesScore

def test_verifier_test_score_zero():
  joueur = TableauDesScore(p1points =0,p2points =0,Avantage_j1 =True,Avantage_j2 =False,winner ="")
  assert joueur.p1points ==0
  assert joueur.p2points ==0

def test_jouter_point_joueur1_quinze():
  joueur = TableauDesScore(p1points=0, p2points=0, Avantage_j1=True, Avantage_j2=False,winner ="")
  joueur.ajouter_point_joueur1()
  assert joueur.p1points == 15


def test_jouter_point_joueur1_trente():
  joueur = TableauDesScore(p1points=15, p2points=0, Avantage_j1=True, Avantage_j2=False,winner ="")
  joueur.ajouter_point_joueur1()
  assert joueur.p1points == 30


def test_jouter_point_joueur1_quarante():
  joueur = TableauDesScore(p1points=30, p2points=0, Avantage_j1=True, Avantage_j2=False,winner ="")
  joueur.ajouter_point_joueur1()
  assert joueur.p1points == 40




def test_jouter_point_joueur2_quinze():
  joueur = TableauDesScore(p1points=0, p2points=0, Avantage_j1=True, Avantage_j2=False,winner= "")
  joueur.ajouter_point_joueur2()
  assert joueur.p2points == 15


def test_jouter_point_joueur2_trente():
  joueur = TableauDesScore(p1points=0, p2points=15, Avantage_j1=True, Avantage_j2=False,winner ="")
  joueur.ajouter_point_joueur2()
  assert joueur.p2points == 30


def test_jouter_point_joueur2_quarante():
  joueur = TableauDesScore(p1points=0, p2points=30, Avantage_j1=True, Avantage_j2=False,winner="")
  joueur.ajouter_point_joueur2()
  assert joueur.p2points == 40



def test_donner_avantage_joueur1():
  joueur = TableauDesScore(p1points=40, p2points=40, Avantage_j1="False", Avantage_j2=False, winner="")
  joueur.ajouter_point_joueur1()
  assert joueur.Avantage_j1 == True




def test_Verifier_joueur1_a_gagner():

  joueur = TableauDesScore(p1points=40, p2points= 40, Avantage_j1=True, Avantage_j2=False, winner="player1")
  joueur.ajouter_point_joueur1()
  assert joueur.winner == "player1"

def test_donner_avantage_joueur2():
  joueur = TableauDesScore(p1points=40, p2points=40, Avantage_j1="False", Avantage_j2=False, winner="")
  joueur.ajouter_point_joueur2()
  assert joueur.Avantage_j2 == True


def test_Verifier_joueur2_a_gagner():
  joueur = TableauDesScore(p1points=40, p2points=40, Avantage_j1=False, Avantage_j2=True, winner="player2")
  joueur.ajouter_point_joueur2()
  assert joueur.winner == "player2"

def test_joueur1_perd_avantage():
  joueur = TableauDesScore(p1points=40, p2points=40, Avantage_j1=True, Avantage_j2=False, winner="")
  joueur.ajouter_point_joueur2()
  assert joueur.Avantage_j1 == False



