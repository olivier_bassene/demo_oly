import random

import unittest

def FizzBuzz(nombre):

    if nombre %3 == 0 and nombre % 5 == 0:
        return "fuzzbuzz"
    elif nombre % 3 ==0:
        return 'fuzz'
    elif nombre % 5 == 0:
        return "buzz"
    else:
        return nombre


def JeuFuzzBuzz(endpoint):
    liste = []

    for i in range(1, endpoint + 1):
        liste.append(FizzBuzz(i))

    return liste

print(JeuFuzzBuzz(100))


class FIZZBUZZ(unittest.TestCase):

    def test_dire_fuzz(self):
        self.assertEqual(FizzBuzz(3),"fuzz")
        self.assertEqual(FizzBuzz(5),'buzz')
        self.assertEqual(FizzBuzz(30),"fuzzbuzz")
        self.assertEqual(JeuFuzzBuzz(6),[1,2,"fuzz",4,"buzz","fuzz"])

'''
class RandomTest(unittest.TestCase):

    def test_choice(self):
        liste = list(range(10))
        elt = random.choice(liste)
        self.assertIn(elt,liste)
    def test_choice_fail(self):

        liste = list(range(10))
        elt = random.choice(liste)
        self.assertIn(elt,('a','b'))
    def test_randon_shuffe(self):
        liste = list(range(10))
        random.choice(liste)
        liste.sort()
        self.assertEqual(liste,list(range(10)))



class Est_PAair(unittest.TestCase):

    def test_est_pair(self):
        self.assertTrue(Pair(2))
        self.assertTrue(Pair(4))
        self.assertTrue(Pair(24))
'''
if __name__ == '__main__':
    unittest.main()


