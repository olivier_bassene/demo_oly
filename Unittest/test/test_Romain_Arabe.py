import unittest

roman_connu = (('M', 1000),
                   ('CM', 900),
                   ('D', 500),
                   ('CD', 400),
                   ('C', 100),
                   ('XC', 90),
                   ('L', 50),
                   ('XL', 40),
                   ('X', 10),
                   ('IX', 9),
                   ('V', 5),
                   ('IV', 4),
                   ('I', 1))
def convertir_entier_roman_base(nombre):
    result = ''
    for roman,entier in roman_connu:
        if nombre == entier:
            return roman
        else:
            while nombre >=entier:
                result+=roman
                nombre  = nombre - entier

    return result

def convertir_romain_to_arabe(chiffre):
    total =0
    for romain,entier in roman_connu:
           while chiffre.startswith(romain):
                total += entier
                chiffre = chiffre[len(romain):]
    return total



class RomanArabe(unittest.TestCase):


    def test_convertir_entier_roman_base(self):
        self.assertEqual(convertir_entier_roman_base(40),'XL')

    def test_convertir_romain_to_arabe(self):
        self.assertEqual(convertir_romain_to_arabe("XIV"),14)
if __name__=="__main__":
    unittest.main()