import unittest
import numpy as np
import math


def est_premier(nombre):
    if nombre < 2:
        return False
    else:
        for i in range(2,math.floor(math.sqrt(nombre)+1)):
            if nombre % i == 0:
                return False
        return True
class Score:

    def __init__(self,score_j1 =0,score_j2 =0,avantage_j1 =False,avantage_j2 =False, winner =''):
        self.score_j1 =score_j1
        self.score_j2 =score_j2
        self.avantage_j1 = avantage_j1
        self.avantage_j2 =avantage_j2
        self.winner =winner

    def add_point_joueur_1(self):
        if self.score_j1 ==0:
            self.score_j1 = 15
        elif self.score_j1 == 15:
            score_j1 = 30
        elif self.score_j1 == 30:
            self.score_j1 = 40
        elif self.score_j1 == 40 and self.avantage_j1 is True:
            self.winner ='joueur 1'



class TennisScore(unittest.TestCase):


    def test_joueur_1_a_quinze(self):
         jouer = Score(score_j1 =40,score_j2 =0,avantage_j1 =True,avantage_j2 =False, winner ='')
         jouer.add_point_joueur_1()
         self.assertEqual(jouer.winner, 'joueur 1')


class Premier(unittest.TestCase):

    def test_nombre_inferieur_a_2_pas_premier(self):

        self.assertFalse(est_premier(9))

if __name__ == "__main__":
    unittest.main()