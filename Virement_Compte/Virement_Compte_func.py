class Compte:

    def __init__(self, num_compte,nom_proprio,adress, solde):

        self.num_compte =  num_compte
        self.nom_proprio = nom_proprio
        self.solde = solde
        self.adress = adress

    def __repr__(self):
        return "{} {} {} {}".format(self.nom_proprio, self.num_compte,self.adress, self.solde)
    @property
    def solde(self):
        return self._solde

    @solde.setter

    def solde(self, value):
        if type(value) is not float and type(value) is not int:
            self._solde = 0
        else:
            self._solde = value

    def faire_virement(self,envoi, receveur, montant):
        receveur.solde = receveur.solde + montant
        envoi.solde =envoi.solde  - montant


if  __name__ =="__main__":
    pass