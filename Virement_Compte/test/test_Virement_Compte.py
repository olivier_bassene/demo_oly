import pytest

from Virement_Compte.Virement_Compte_func import Compte

def test_verifier_nom_bien_renseigner():

    #Given
    expected = "olivier"
    compte = Compte(num_compte =1456,nom_proprio = expected,adress = "Paris", solde = 20)
    #When
    actual = compte.nom_proprio
    #Then
    assert actual == expected

def test_verifier_numero_bien_renseigner():

    #Given
    expected = 80097
    compte = Compte(num_compte =expected,nom_proprio = "moi",adress = "Paris", solde = 10)
    #When
    actual = compte.num_compte
    #Then
    assert actual == expected

def test_faire_virement():
    # Given
    montant = 20
    compte_denvoi = Compte(num_compte=67889, nom_proprio="moi", adress="Paris", solde=200)
    compte_receveur = Compte(num_compte=223, nom_proprio="celestin", adress="Paris", solde=0)
    expected = compte_denvoi.solde - montant
    # When
    compte_denvoi.faire_virement(compte_denvoi,compte_receveur,montant)
    actual = compte_denvoi.solde
    # Then
    assert actual == expected



def test_recevoir_virement():
    # Given
    montant = 20
    compte_denvoi = Compte(num_compte=67889, nom_proprio="moi", adress="Paris", solde=200)
    compte_receveur = Compte(num_compte=223, nom_proprio="celestin", adress="Paris", solde=0)
    expected = montant
    # When
    compte_denvoi.faire_virement(compte_denvoi,compte_receveur,montant)
    actual = compte_receveur.solde
    # Then
    assert actual == expected
